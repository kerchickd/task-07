package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;

	private final String URL;
	private static final Logger LOGGER = Logger.getLogger(DBManager.class.getName());

	private static final String SELECT_ALL_USERS =
			"SELECT * FROM users";
	private static final String SELECT_ALL_TEAMS =
			"SELECT * FROM teams";
	private static final String INSERT_USER =
			"INSERT INTO users VALUES (DEFAULT, ?)";
	private static final String INSERT_TEAM =
			"INSERT INTO teams VALUES (DEFAULT, ?)";
	private static final String GET_USER =
			"SELECT * FROM users WHERE login = ?";
	private static final String GET_TEAM =
			"SELECT * FROM teams WHERE name = ?";
	private static final String DELETE_USERS =
			"DELETE FROM users WHERE login = ?";
	private static final String DELETE_TEAM =
			"DELETE FROM teams WHERE name = ?";
	private static final String UPDATE_TEAM =
			"UPDATE teams SET name = ? WHERE id = ?";
	private static final String GET_USER_TEAMS =
					"SELECT t.id, t.name FROM teams t " +
					"LEFT JOIN users_teams u ON t.id = u.team_id " +
					"WHERE u.user_id = ?";
	private static final String INSERT_TEAM_USER_PAIR =
			"INSERT INTO users_teams VALUES (?, ?)";


	public static synchronized DBManager getInstance() {
		if (instance == null)
			instance = new DBManager();

		return instance;
	}

	private DBManager() {
		Properties prop = new Properties();
		StringBuilder temp = new StringBuilder();
		try (InputStream input = new FileInputStream("app.properties")) {
			prop.load(input);
			temp.append(prop.getProperty("connection.url"));
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "Fail in prop. file: ", e);
		}
		URL = temp.toString();
	}

	public List<User> findAllUsers() throws DBException {
		List<User> allUsers = new ArrayList<>();
		try (Connection con = DriverManager.getConnection(URL);
			Statement statement = con.createStatement()) {
			ResultSet rs = statement.executeQuery(SELECT_ALL_USERS);
			while (rs.next()) {
				int id = rs.getInt("id");
				String login = rs.getString("login");
				User user = User.createUser(login);
				user.setId(id);
				allUsers.add(user);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.SEVERE, "Exception in find method!", e);
			throw new DBException("Exception in find method!", e);
		}
		return allUsers;
	}

	public boolean insertUser(User user) throws DBException {
		if (getUser(user.getLogin()) != null)
			return false;
		try (Connection con = DriverManager.getConnection(URL);
			 PreparedStatement statement =
					 con.prepareStatement(INSERT_USER,
							 Statement.RETURN_GENERATED_KEYS))  {
			statement.setString(1, user.getLogin());
			if (statement.executeUpdate() > 0) {
				ResultSet set = statement.getGeneratedKeys();
				if (set.next())
					user.setId(set.getInt(1));
			}
		} catch (SQLException e) {
			LOGGER.log(Level.SEVERE, "Exception in insert method!", e);
			throw new DBException("Exception in insert method!", e);
		}
		return true;
	}

	public boolean deleteUsers(User... users) throws DBException {
		boolean result = true;
		try (Connection con = DriverManager.getConnection(URL);
			 PreparedStatement statement =
					 con.prepareStatement(DELETE_USERS)) {
			for (var x : users) {
				statement.setString(1, x.getLogin());
				result = result && statement.executeUpdate() > 0;
			}
		} catch (SQLException e) {
			LOGGER.log(Level.SEVERE, "Exception in delete method!", e);
			throw new DBException("Exception in delete method!", e);
		}
		return result;
	}

	public User getUser(String login) throws DBException {
		User user = null;
		try (Connection con = DriverManager.getConnection(URL);
		    PreparedStatement statement = con.prepareStatement(GET_USER)) {
			statement.setString(1, login);
			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				user = User.createUser(login);
				user.setId(rs.getInt("id"));
			}
		} catch (SQLException e) {
			LOGGER.log(Level.SEVERE, "Exception in get method!", e);
			throw new DBException("Exception in get method!", e);
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = null;
		try (Connection con = DriverManager.getConnection(URL);
			 PreparedStatement statement = con.prepareStatement(GET_TEAM)) {
			statement.setString(1, name);
			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				team = Team.createTeam(name);
				team.setId(rs.getInt("id"));
			}
		} catch (SQLException e) {
			LOGGER.log(Level.SEVERE, "Exception in get method!", e);
			throw new DBException("Exception in get method!", e);
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> allTeams = new ArrayList<>();
		try (Connection con = DriverManager.getConnection(URL);
			 Statement statement = con.createStatement()) {
			ResultSet rs = statement.executeQuery(SELECT_ALL_TEAMS);
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				Team team = Team.createTeam(name);
				team.setId(id);
				allTeams.add(team);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.SEVERE, "Exception in find method!", e);
			throw new DBException("Exception in find method!", e);
		}
		return allTeams;
	}

	public boolean insertTeam(Team team) throws DBException {
		if (getTeam(team.getName()) != null)
			return false;
		try (Connection con = DriverManager.getConnection(URL);
			 PreparedStatement statement =
					 con.prepareStatement(INSERT_TEAM,
							 Statement.RETURN_GENERATED_KEYS))  {
			statement.setString(1, team.getName());
			if (statement.executeUpdate() > 0) {
				ResultSet set = statement.getGeneratedKeys();
				if (set.next())
					team.setId(set.getInt(1));
			}
		} catch (SQLException e) {
			LOGGER.log(Level.SEVERE, "Exception in insert method!", e);
			throw new DBException("Exception in insert method!", e);
		}
		return true;
	}

	private void setTeamUserPair (Connection con, User user, Team team) throws DBException {
		try (PreparedStatement statement =
					 con.prepareStatement(INSERT_TEAM_USER_PAIR)) {
			statement.setInt(1, user.getId());
			statement.setInt(2, team.getId());
			statement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.SEVERE, "Failed set pair! ", e);
			try {
				con.rollback();
				con.close();
				throw new DBException("Exception pair!", e);
			} catch (SQLException ex) {
				throw new DBException("Something went wrong!", ex);
			}
		}
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection con = null;
		try {
			con = DriverManager.getConnection(URL);
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			for (Team t : teams)
				setTeamUserPair(con, user, t);
			con.commit();
		} catch (SQLException e) {
			LOGGER.log(Level.SEVERE, "Exception, maybe some pair add twice!", e);
			try {
				if (con != null)
					con.rollback();
			} catch (SQLException ex) {
				throw new DBException("Something went wrong!", ex);
			}
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				throw new DBException("Something went wrong!", e);
			}
		}
		return true;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> userTeams = new ArrayList<>();
		User userFromDB = getUser(user.getLogin());
		if (userFromDB == null)
			return new ArrayList<>();
		try (Connection con = DriverManager.getConnection(URL);
			PreparedStatement statement = con.prepareStatement(GET_USER_TEAMS)) {
			statement.setInt(1, userFromDB.getId());
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				Team addTeam = Team.createTeam(rs.getString("name"));
				addTeam.setId(rs.getInt("id"));
				userTeams.add(addTeam);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.SEVERE, "Exception in get user's team method!", e);
			throw new DBException("Exception in get user's team method!", e);
		}
		return userTeams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		boolean result;
		try (Connection con = DriverManager.getConnection(URL);
			 PreparedStatement statement =
					 con.prepareStatement(DELETE_TEAM)) {
			statement.setString(1, team.getName());
			result = statement.executeUpdate() > 0;
		} catch (SQLException e) {
			LOGGER.log(Level.SEVERE, "Exception in delete method!", e);
			throw new DBException("Exception in delete method!", e);
		}
		return result;
	}

	public boolean updateTeam(Team team) throws DBException {
		boolean result;
		try (Connection con = DriverManager.getConnection(URL);
			 PreparedStatement statement =
					 con.prepareStatement(UPDATE_TEAM)) {
			statement.setString(1, team.getName());
			statement.setInt(2, team.getId());
			result = statement.executeUpdate() > 0;
		} catch (SQLException e) {
			LOGGER.log(Level.SEVERE, "Exception in update method!", e);
			throw new DBException("Exception in update method!", e);
		}
		return result;
	}
}
